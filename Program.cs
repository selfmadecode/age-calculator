﻿using System;

namespace ConsoleHelloApp
{
    // Author = SelfMadeCode (14 July, 2020)
    // Permission to use and or edit this code has been granted by the author
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "Hello! World";
            userDetails();
        }
        static void userDetails()
        {
            string fullName;
            string yearOfBirth;
            var today = DateTime.Today; // Get the current year from the system Date and Time
            //int age;
            
            Console.WriteLine("Hello, can we meet you!");
            Console.Write("What is your full name: ");
            fullName = Console.ReadLine();
            Console.WriteLine("Type in your Date of Birth in DD-MM-YYYY format:");
            var Bday = Console.ReadLine();
            var myDate = Convert.ToDateTime(Bday);
            var age = today.Year - myDate.Year;
            if (myDate > today)
            {
                Console.WriteLine("Incorrect Data! Birth cannot be in the future");
                return;
            }
            
            Console.WriteLine(greeting(fullName, age));
            Console.ReadLine();


            
        }

        static string greeting(string fullName, int age)
        {
            return "Hello " + fullName + ", you are " + age + " years old today!";
        }
    }
}
